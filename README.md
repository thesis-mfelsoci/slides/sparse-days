# Direct solution of larger coupled sparse/dense FEM/BEM linear systems using low-rank compression

[![pipeline status](https://gitlab.inria.fr/thesis-mfelsoci/slides/sparse-days/badges/master/pipeline.svg)](https://gitlab.inria.fr/thesis-mfelsoci/slides/sparse-days/-/commits/master)

In the aeronautical industry, aeroacoustics is used to model the propagation of
acoustic waves in air flows enveloping an aircraft in flight. This for instance
allows one to simulate the noise produced at ground level by an aircraft during
the takeoff and landing phases, in order to validate that the regulatory
environmental standards are met. Unlike most other complex physics simulations,
the method resorts to solving coupled sparse/dense systems. Indeed, the
heterogeneity of the jet flow created by reactors often requires a Finite
Element Method (FEM) discretization, leading to a sparse linear system, while it
may be reasonable to assume as homogeneous the rest of the space and hence model
it with a Boundary Element Method (BEM) discretization, leading to a dense
system. In an industrial context, these simulations are often operated on modern
multicore workstations with fully-featured linear solvers. Exploiting their
low-rank compression techniques is thus very appealing for solving larger
coupled sparse/dense systems (hence ensuring a finer solution) on a given
multicore workstation, and – of course – possibly do it fast. The standard
method performing an efficient coupling of sparse and dense direct solvers is to
rely on the Schur complement functionality of the sparse direct solver. However,
to the best of our knowledge, modern fully-featured sparse direct solvers
offering this functionality return the Schur complement as a non compressed
matrix.  We have studied the opportunity to process larger systems in spite of
this constraint. For that we propose two classes of algorithms, namely
multi-solve and multi-factorization, consisting in composing existing parallel
sparse and dense methods on well chosen submatrices. An experimental study
conducted on a 24 cores machine equipped with 128 GiB of RAM shows that these
algorithms, implemented on top of state-of-the-art sparse and dense direct
solvers, together with proper low-rank assembly schemes, can respectively
process systems of 9 million and 2.5 million total unknowns instead of 1.3
million unknowns with a standard coupling of compressed sparse and dense
solvers. We are currently extending this work to the out-of-core and
distributed-memory cases. Moreover, we are working on a task-based
implementation scheme allowing for a better inter-operability between the sparse
and the dense solver and aiming at eliminating the current limitations of the
multi-solve and multi-factorization schemes.

[Slides for Sparse Days 2022](https://thesis-mfelsoci.gitlabpages.inria.fr/slides/sparse-days/sparse-days.pdf)

[Event](https://sparsedays.cerfacs.fr/)
