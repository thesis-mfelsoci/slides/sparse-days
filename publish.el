(require 'org) ;; Org mode support
(require 'htmlize) ;; source code block export to HTML
(require 'ox-publish) ;; publishing functions
(require 'ox-latex) ;; LaTeX publishing functions
(require 'org-ref) ;; bibliography support

;; Enable Babel code evaluation.
(setq org-export-babel-evaluate t)

;; Load languages for code block evaluation.
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)))

;; Load style presets.
(load-file "styles/styles.el")

;; Configure HTML website and PDF document publishing.
(setq org-publish-project-alist
      (list
       (list "sparse-days"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["sparse-days.org"]
             :publishing-function '(org-beamer-publish-to-pdf)
             :publishing-directory "./public")       
       (list "sparse-days-figures"
             :base-directory "."
             :base-extension "org"
             :exclude ".*"
             :include ["sparse-days.org"]
             :publishing-function '(org-babel-execute-file)
             :publishing-directory "./public")))

(provide 'publish)
