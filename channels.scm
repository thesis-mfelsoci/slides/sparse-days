(list
 (channel
  (name 'guix)
  (url "https://git.savannah.gnu.org/git/guix.git")
  (commit "9235dd136e70dfa97684aff4e9af4c0ce366ad68"))
 (channel
  (name 'guix-extra)
  (url "https://gitlab.inria.fr/mfelsoci/guix-extra.git")
  (commit "35cdd19f80516ad7b89a68be5184910719324aca")))
